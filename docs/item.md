## Item

`Item` Endpoint for managaing items.

### Endpoint Summary

-   `[GET]` /api/items - [Retrieve All *Items*](#Retrieve-All-Items)
-   `[POST]` /api/item - [Create a new *Item*](#Create-a-new-Item)
-   `[GET]` /api/item/<.id> - [Retrieve a single *Item* with `id`](#Retrieve-a-single-Item)
-   `[PUT]` /api/item/<.id> - [Edit a single *Item* with `id`](#Edit-a-single-Item)
-   `[DELETE]` /api/item/<.id> - [Delete a single *Item* with `id`](#Delete-a-single-Item)
-   `[GET]` /api/items/search - [Searches all *items* for multiple values](#Search-items)
-   `[POST]` /api/items/search - [Applies Advance search(like ranges, arrays) over *items* for multiple values](#Search-items)
-   `[GET]` /api/items/test - [Quick Test item](#Quick-Test-item)
-   `[POST]` /api/items - [Create bulk *Item*](#Create-bulk-Item)
-   `[PUT]` /api/items - [Edits bulk *Item* with conditions](#Edit-bulk-Item)
-   `[DELETE]` /api/items - [Delete bulk *items* in the collection](#Delete-bulk-items)




**N.B**: The `/test` endpoint of this item is for quick development testing only. Do Disable this when in production!

### SDK Summary

    - Unity >= 5
    - Angular >= 4.3

The SDKs have provider code already set

### Retrieve All Items

-   **Syntax** : `[GET] /api/items [?skip= X & limit= Y]`
-   **URL** : `/api/items`
-   **Method**: `GET`
-   **URL Params**:  
     **Required:** None  
     **Optional:**

    `skip=[Integer]` - Offsets(Skips) index of results  
     `limit=[Integer]` - Total number of results in the current request to return

-   **Success Response:**

    **Code:** 200 <br />
    **Content:**

    ```
    {
      "status": "success",
      "data": {
        "items ": [
          {"_id":"56cb91bdc3464f14678934ca","name":"lorem pisum dolor sit amet","price":3.5},
          .
          .
          .
        ],
        "count": 1
      },
      "message": null
    }
    ```

-   **Sample Call:**

    `curl "http://localhost:3000/api/items"`  
    Fetches 5 item results skipping the first 2

-   **Notes:**

### Create a new Item

-   **Syntax** : `[POST] /api/item`
-   **URL** : `/api/item`
-   **Method**: `POST`
-   **URL Params**:  
     **Optional:** None  
     **Required:**

    `{item:{}}` - The base item data object

    ```
     {
       "item" : {
        {"_id":"56cb91bdc3464f14678934ca","name":"lorem pisum dolor sit amet","price":3.5}
       }
     }
    ```

-   **Success Response:**

    **Code:** 201  
     **Content:**

    ```
      {
        "status": "success",
        "data": {
          "__v": 0,
          "_id": "58713aaf1657a2bd9c5a00e0",
          name : String, 
          price : Number
          
        },
        "message": null
      }
    ```

-   **Error Response:**

    **Code:** 500 <br />
    **Content:**

    ```
      {
        "status": "error",
        "data": "Invalid item/key model provided",
        "message": "There was an error saving this data."
      }
    ```

-   **Sample Call:**

    ```
        curl -X POST -H "Content-Type: application/json"
          -H "Cache-Control: no-cache" -d     '{
          "item":{
              "name":"pen",
              "price":2.54
              }
          }' "http://localhost:3000/api/item"

    ```

    The key model being `item` the saves a 'pen' data

-   **Notes:**

### Retrieve a single Item

-   **Syntax** : `[GET] /api/item/:id`
-   **URL** : `/api/item/:id`
-   **Method**: `GET`
-   **URL Params**:  
     **Optional:** None  
     **Required:**

    `id` - The object id of the item  


-   **Success Response:**

    **Code:** 200  
     **Content:**

    ```
      {
        "status": "success",
        "data": {
          "_id": "587100001657a2bd9c5a00df",
          "__v": 0,
          name : String, 
          price : Number
          
        },
        "message": null
      }
    ```

-   **Error Response:**

    **Code:** 404  
     **Content:**

    ```
      {
        "status": "error",
        "data": 404,
        "message": "Not Found Error"
      }
    ```

-   **Sample Call:**

    ```
        curl -X GET -H "Content-Type: application/json"
          -H "Cache-Control: no-cache"
          "http://localhost:3000/api/item/587100001657a2bd9c5a00d"

    ```

    Fetches a single item from the collection `items`

-   **Notes:**

### Edit a single Item

-   **Syntax** : `[PUT] /api/item/:id`
-   **URL** : `/api/item/:id`
-   **Method**: `PUT`
-   **URL Params**:  
     **Optional:** None  
     **Required:**

    `id` - The object id of the item  
     `{item:{}}` - The base item data object that needs to be changed

    ```
     {
       "item" : {
         name : String, 
         price : Number
         
       }
     }
    ```

-   **Success Response:**

    **Code:** 202  
     **Content:**

    ```
      {
        "status": "success",
        "data": {
          "_id": "587100001657a2bd9c5a00df",
          "__v": 0,
          name : String, 
          price : Number
          
        },
        "message": null
      }
    ```

-   **Error Response:**

    **Code:** 500  
     **Content:**

    ```
      {
        "status": "error",
        "data": "Invalid item/key model provided",
        "message": "There was an error updating this data."
      }
    ```

    **Code:** 404  
     **Content:**

    ```
    {
      "status": "error",
      "data": 404,
      "message": "No Data Found"
    }
    ```

-   **Sample Call:**

    ```
        curl -X PUT -H "Content-Type: application/json"
          -H "Cache-Control: no-cache"
          -d '{
                "item22":{
                    "name":"sharpner",
                    "price":2.55
                  }
              }' "http://localhost:3000/api/item/587100001657a2bd9c5a00df"

    ```

    The key model being `item` which updates a 'sharpner' data

-   **Notes:**

### Delete a single Item

-   **Syntax** : `[DELETE] /api/item/:id`
-   **URL** : `/api/item/:id`
-   **Method**: `DELETE`
-   **URL Params**:  
     **Optional:** None  
     **Required:**

    `id` - The object id of the item

-   **Success Response:**

    **Code:** 202  
     **Content:**

    ```
    {
      "status": "success",
      "data": "The item got Deleted",
      "message": null
    }
    ```

-   **Error Response:**

    **Code:** 500  
     **Content:**

    ```
      {
      "status": "error",
      "data": "Error in deleting this item",
      "message": {
        .
        .
        .
      }
    }
    ```

-   **Sample Call:**

    ```
      curl -X DELETE "http://localhost:3000/api/item/58713b0a1657a2bd9c5ad"
    ```

    The key model being `item` which updates a 'sharpner' data

-   **Notes:**

### Delete all Items

-   **Syntax** : `[DELETE] /api/items`
-   **URL** : `/api/items`
-   **Method**: `DELETE`
-   **URL Params**:  
     **Optional:** None  
     **Required:** None
-   **Success Response:**

    **Code:** 202  
     **Content:**

    ```
     {
       "status": "success",
       "data": "All items got Delete",
       "message": null
     }
    ```

-   **Error Response:**

    **Code:** 500  
     **Content:**

    ```
       {
         "status": "error",
         "data": "Error in deleting all items",
         "message": {
           .
           .
           .
         }
       }
    ```

-   **Sample Call:**

    ```
      curl -X DELETE "http://localhost:3000/api/items"
    ```

    The key model being `item` which updates a 'sharpner' data

-   **Notes:**

### Search Items

-   **Syntax** : `[GET] /api/items/search [?skip= X & limit= Y & keyword= field:value [,field:value]]`
-   **URL** : `/api/items/search`
-   **Method**: `GET`
-   **URL Params**:  
     **Required:** keyword  
     **Optional:**

    `skip=[Integer]` - Offsets(Skips) index of results  
     `limit=[Integer]` - Total number of results in the current request to return
    `keyword=[CSV]` - keyword = field1:value1, filed2:value2 ...
    `strict=[Boolean]` - Performs Strict search.

-   **Success Response:**

    **Code:** 200 <br />
    **Content:**

    ```
    {
      "status": "success",
      "data": {
        "items": [
          {
            "_id": "587100001657a2bd9c5a00df",
            name : String,
        price : Number,
            "__v": 0
          },
          .
          .
          .
        ],
        "count": 1
      },
      "message": null
    }
    ```

-   **Sample Call:**

    `curl "http://localhost:3000/api/items/search?keyword=first:Sam,last:Jones"`  
    Searches items with rows with its first name 'Sam' and last name 'Jones'

-   **Notes:**
    To use Strict Search, add param ?strict=true
