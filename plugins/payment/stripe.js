const debug = require('debug')('App:Plugin:Payment');
const uuidv4 = require('uuid/v4');
const _ = require('lodash');

const API = {};

API.Config = {};
API.sharedConfig = {};
API.sdk = {};

API.Init = async (Config, sharedConfig) => {
  API.sharedConfig = sharedConfig;
  API.Config = Config;

  API.sdk = require('stripe')(Config.secret);
};

// Sugar-coated process.
API.QuickPay = async (user, token, amount, description) => {
  const charge = await API.sdk.charges.create({
    amount: amount * API.sharedConfig.multiplier,
    currency: API.sharedConfig.currency,
    source: token,
    description: description || `Charged ${API.sharedConfig.currency} ${amount * API.sharedConfig.multiplier} by Stripe`,
    receipt_email: user.email
  }, {
    idempotency_key: uuidv4()
  });

  return _.pick(charge, ['id', 'amount', 'balance_transaction', 'charge', 'receipt_email', 'receipt_url', 'status']);
};

module.exports = API;
