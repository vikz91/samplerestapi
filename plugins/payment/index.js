
const API = {};

API.Config = {};
API.provider = {};

API.Init = async (Config) => {
  API.Config = Config;
  API.UseSystem(Config.defaultSystem);
};

API.UseSystem = (system) => {
  API.provider = require('./' + system);

  return API.provider.Init(API.Config.systems[system], API.Config.shared);
};

// user: {email:''}
API.QuickPay = async (user, token, amount, description) => {
  return API.provider.QuickPay(user, token, amount, description);
};

module.exports = API;
