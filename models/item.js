'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const fields = {

  name: {
    type: String
  },

  price: {
    type: Number
  }

};

const ModelSchema = new Schema(fields, { timestamps: true });
module.exports = mongoose.model('Item', ModelSchema);
