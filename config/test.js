'use strict';

var config = require('./development');

//  ======[ DATABASE ]======

config.db.credential = {
  database: 'restologic-test',
  host: 'localhost',
  user: '',
  pw: '',
  port: 27017
};
config.db.cluster = false; // use cluster

config.db.options.createIndexes = false;
config.db.options.poolSize = 5;

//  ======[ ADDRESS ]======
config.serverIp = '0.0.0.0';
config.serverPort = 3000;

//  ======[ ADMIN ]======
config.admin.resetPasswordHost = 'localhost';
config.admin.resetPasswordRoute = null;
config.admin.resetPasswordEmail = 'admin@local.com';
config.admin.errorEmail = 'admin@local.com';

//  ======[ API ]======
config.JWT.secret = 'simpleSecret';

//  ======[ MAIL SERVICE ]======
config.mailService.systems.mailer = {
  service: 'Gmail',
  user: 'someone@gmail.com',
  pass: 'nopass'
};

config.mailService.systems.sendgrid = '[MOCK SENDGRID API KEY]';

config.mailService.systems.mailgun = {
  apiKey: '[Your_Mailgun_key]',
  domain: '[Your_website_domain]'
};

//  ======[ ERROR STACK ]======
config.errorStack = {
  options: {
    dumpExceptions: true,
    showStack: true
  },
  viewPretty: true
};

module.exports = config;
