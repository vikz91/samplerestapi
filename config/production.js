'use strict';

var config = require('./development');

//  ======[ DATABASE ]======

config.db.credential = {
  database: 'restologic',
  host: 'mongo',
  user: '',
  pw: '',
  port: 27017
};

config.db.cluster = false; // use cluster

config.db.options.useCreateIndex = false;
config.db.options.poolSize = 10;
config.db.options.reconnectTries = Number.MAX_VALUE; // Never stop trying to reconnect
config.db.options.reconnectInterval = 500; // Reconnect every 500ms

config.redis = {
  database: 'restologic',
  host: 'redis',
  user: '',
  pw: '',
  port: 6379,
  opts: {}
};

//  ======[ ADDRESS ]======
config.serverIp = '0.0.0.0';
config.serverPort = 3000;

//  ======[ ADMIN ]======
config.admin.resetPasswordHost = 'localhost';
config.admin.resetPasswordRoute = null;
config.admin.resetPasswordEmail = 'admin@local.com';
config.admin.errorEmail = 'admin@local.com';

//  ======[ API ]======
config.JWT.secret = 'this.is.5|_|p3R.#$@~S3(R3T~@$#.D0.(H^NG3.TH15';

//  ======[ MAIL SERVICE ]======
config.mailService.systems.mailer = {
  service: 'Gmail',
  user: '[Your_Gmail_id]@gmail.com',
  pass: '[Your_Gmail_pass]'
};

config.mailService.systems.sendgrid = '[MOCK SENDGRID API KEY]';

config.mailService.systems.mailgun = {
  apiKey: '[Your_Mailgun_key]',
  domain: '[Your_website_domain]'
};

//  ======[ ERROR STACK ]======
config.errorStack = {
  options: {
    dumpExceptions: false,
    showStack: false
  },
  viewPretty: false
};

module.exports = config;
