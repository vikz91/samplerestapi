'use strict';
// Module dependencies.
const mongoose = require('mongoose');
const Model = mongoose.models.Item;
const Util = require('../library').Util;
// const debug = require('debug')('App:ApiObject:item')
const csvReader = require('csvtojson');

const ModelOptions = {
  mutable: ['name', 'description', 'price'],
  search: ['name', 'description', 'price']
};
const { Storage, Email, Payment } = require('../plugins');
/*
========= [ CORE METHODS ] =========
*/

// GET
exports.Read = (id) => {
  return Model.findOne({
    _id: id
  })
    .then(data => {
      if (!data) {
        return Promise.reject(new Error(404));
      }

      return data;
    });
};

// POST
exports.Create = (data) => {
  data = new Model(data);
  return data.save()
    .then(() => data.toObject());
};

// PUT
exports.Update = (id, updateData) => {
  const finalData = {};
  ModelOptions.mutable.forEach(prop => {
    if (typeof updateData[prop] !== 'undefined') {
      finalData[prop] = updateData[prop];
    }
  });

  return Model.findOneAndUpdate({
    _id: id
  }, {
    $set: finalData
  }, {
    new: true
  })
    .then(data => {
      return data.toObject() || null;
    }); // eo Model.findOneAndUpdate
};

// DELETE
exports.Delete = (id) => {
  return Model.deleteOne({
    _id: id
  }).then(() => {
    return 'The data got Deleted';
  });
};

/*
========= [ BULK METHODS ] =========
*/

// ALL
exports.ReadList = (listOptions) => {
  const res = {
    data: {},
    count: 0
  };

  // return Promise.reject(new Error('Fake Error')) // Quick Err Check

  return Model.find()
    .limit(listOptions.limit)
    .skip(listOptions.skip)
    .sort({ createdAt: listOptions.sort })
    .exec()
    .then((list) => {
      res.data = list;
      return Model.countDocuments();
    })
    .then(count => {
      res.count = count;
      return res;
    });
};

// BULK ADD
exports.CreateBulk = (file) => {
  const csvFilePath = file.path;
  return csvReader()
    .fromFile(csvFilePath)
    .then(jsonData => Model.insertMany(jsonData))
    .then(insertData => Util.ProcessCountResponseMap(insertData));
};

// BULK EDIT
exports.UpdateBulk = (condition, file) => {
  const csvFilePath = file.path;
  return csvReader()
    .fromFile(csvFilePath)
    .then(jsonData => Model.updateMany(condition, jsonData))
    .then(insertData => Util.ProcessCountResponseMap(insertData));
};

// BULK DELETE
exports.DeleteBulk = () => {
  return Model.remove({}).then(() => 'All data got Deleted');
};

/*
========= [ SEARCH METHODS ] =========
*/

// SEARCH
exports.Search = (listOptions, keywordObj, strict) => {
  let k = {};

  if (strict) {
    k = keywordObj;
  } else {
    Object.keys(keywordObj).forEach(key => {
      if (isNaN(keywordObj[key])) {
        k[key] = new RegExp(keywordObj[key], 'i');
      } else {
        k[key] = keywordObj[key];
      }
    });
  }

  return Model.find(k)
    .limit(listOptions.limit)
    .skip(listOptions.skip)
    .sort({ createdAt: listOptions.sort })
    .exec();
};

// SEARCH ADVANCED
exports.SearchAdvanced = (listOptions, data) => {
  const searchObj = [];

  ModelOptions.search.forEach(prop => {
    if (typeof data[prop] !== 'undefined') {
      // let negate = data[prop].negate || false;

      switch (data[prop].search) {
        case 'single':
          searchObj.push({
            [prop]: new RegExp(data[prop].value, 'i')
          });
          break;
        case 'range':
          searchObj.push({
            [prop]: {
              $gte: data[prop].value[0] || Number.MAX_SAFE_INTEGER
            }
          });

          searchObj.push({
            [prop]: {
              $lte: data[prop].value[1] || Number.MIN_SAFE_INTEGER
            }
          });
          break;
        case 'array':
          searchObj.push({
            [prop]: {
              $in: [].concat(data[prop].value)
            }
          });
          break;
      }
    }
  });

  // debug('Modelfields.search: %o', ModelOptions.search);
  // debug('data: %o', data);
  // debug('searchObj: %o', searchObj);

  if (!searchObj || searchObj.length === 0) {
    return Promise.resolve([]);
  }

  return Model.find({
    $and: searchObj
  })
    .skip(listOptions.skip)
    .limit(listOptions.limit)
    .sort({ createdAt: listOptions.sort })
    .exec();
};

/*
========= [ FILE METHODS ] =========
*/

exports.UploadFile = (prefix, file) => {
  return Storage.Upload(prefix, file);
};

exports.GetFileList = async (prefix) => {
  let rawList = await Storage.List(prefix);
  rawList = rawList ? rawList[0] : null;

  const projection = ['id', 'name'];
  const ans = rawList.map(ar => {
    const obj = {};
    Object.keys(ar)
      .filter(x => projection.indexOf(x) >= 0)
      .forEach(x => { obj[x] = ar[x]; });
    return obj;
  });

  return ans;
};

exports.GetFileInfo = (prefix) => {
  return Storage.Info(prefix);
};

exports.DeleteFile = (prefix) => {
  return Storage.Delete(prefix);
};

exports.DownloadFile = async (prefix, res) => {
  return Storage.Download(prefix, res);
};

/*
========= [ EMAIL METHODS ] =========
*/
exports.SendEmail = async (id) => {
  const subject = `Job @ Google (${new Date().getHours()} : ${new Date().getMinutes()})`;
  return Email.SendMail(new Email.MailOptions('vikz91.deb@gmail.com', subject, 'Congrats Deb. You got a job in Google. ' + id));
};

/*
========= [ PAYMENT METHODS ] =========
*/
exports.Purchase = async (id, token) => {
  return Payment.QuickPay({ email: 'vikz91.deb@gmail.com' }, token, 20);
};
