using System;
using SimpleJSON;
using UnityEngine;

[Serializable]
public class ModelItem {

    public string _id;

    public string createdAt;
    public string updatedAt;

     
	public string name;  
	public string price; 

    public ModelItem() {}

    public ModelItem(JSONNode data) {
        this._id = data["_id"];

        this.createdAt = data["createdAt"];
        this.updatedAt = data["updatedAt"];

		 
		this.name = data["name"];  
		this.price = data["price"]; 
    }

    public string ToJSON() {
        //return JsonUtility.ToJson(this);
        JSONNode data = JSON.Parse("{}");

		 
		data["name"] = this.name;  
		data["price"] = this.price; 
	
        return data.ToString();
    }

}

[Serializable]
public class ModelItemFileUpload {
    public string file;

    public ModelItemFileUpload(JSONNode data) {
        this.file = data["file"];
    }
}
